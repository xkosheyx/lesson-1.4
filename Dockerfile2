FROM debian:11 as build

RUN apt update && apt install -y wget gcc make libpcre++-dev
RUN wget https://github.com/openresty/luajit2/archive/refs/tags/v2.1-20230410.tar.gz && tar xfvz v2.1-20230410.tar.gz && cd luajit2-2.1-20230410 && make && make install
RUN wget https://github.com/openresty/lua-resty-core/archive/refs/tags/v0.1.26.tar.gz && tar xfvz v0.1.26.tar.gz
RUN wget https://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v0.13.tar.gz && tar xfvz v0.13.tar.gz && cp -r lua-resty-lrucache-0.13/lib lua-resty-core-0.1.26/
RUN wget https://nginx.org/download/nginx-1.23.4.tar.gz && tar xvfz nginx-1.23.4.tar.gz
RUN wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.2.tar.gz && wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.24.tar.gz && tar xvfz v0.3.2.tar.gz && tar xvfz v0.10.24.tar.gz
RUN cd nginx-1.23.4 && export LUAJIT_LIB=/usr/local/lib && export LUAJIT_INC=../luajit2-2.1-20230410/src && ./configure --without-http_gzip_module  --with-ld-opt="-Wl,-rpath,/usr/lib" --add-module=../ngx_devel_kit-0.3.2 --add-module=../lua-nginx-module-0.10.24 && make && make install

FROM debian:11
RUN apt update && apt install -y make
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
COPY --from=build /usr/local/lib/libluajit-5.1.so.2 /usr/lib/x86_64-linux-gnu/libluajit-5.1.so.2
COPY --from=build /lua-resty-core-0.1.26 /lua-resty-core
RUN cd /lua-resty-core && make install
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
